%global forgeurl https://github.com/BelledonneCommunications/bcunit
%global commit 02f7d73e1ef2a67af81b36a210ad56887a117153
%global date 20210211
%forgemeta

Name:           BCUnit
Version:        3.0.2
Release:        0.1%{?dist}
Summary:        Unit testing framework for C

License:        LGPLv2+
URL:            %{forgeurl}
Source0:        %{forgesource}
Patch0:         %{name}-cmake-path.patch

BuildRequires:  cmake
BuildRequires:  gcc

%description
BCUnit is fork of CUnit, a lightweight system for writing, administering, and
running unit tests in C. It provides C programmers a basic testing
functionality with a flexible variety of user interfaces.

%package devel
Summary:        Header files and libraries for CUnit development
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains the header files
and libraries for use with BCUnit package.


%prep
%forgesetup
%patch0 -p1


%build
%cmake -DENABLE_DOC=ON -DENABLE_STATIC=OFF
%cmake_build


%install
%cmake_install
find %{buildroot} -name *.la -delete

%ldconfig_scriptlets


%files
%{_datadir}/%{name}/
# beware full version is used in the SONAME
%{_libdir}/libbcunit.so.1.0.1
%dir %{_docdir}/%{name}
%license COPYING
%doc AUTHORS
%doc ChangeLog
%doc NEWS
%doc README.md
%doc TODO
%doc VERSION

%files devel
%{_docdir}/%{name}/
%{_includedir}/%{name}/
%{_libdir}/libbcunit.so
%{_libdir}/pkgconfig/bcunit.pc
%{_mandir}/man3/BCUnit.3*


%changelog
* Sun May 23 2021 František Dvořák <valtri@civ.zcu.cz> - 3.0.2-0.1.20210211git02f7d73
- Initial version
